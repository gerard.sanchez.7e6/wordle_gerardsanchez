# **Wordle**

### **Actualitzacions**
- Introducció d'un menú principal on l'usuari pot navergar per la terminal.

- 2 idiomes en total. Ara ja es pot jugar també en Castellà, abans només en Català. 

- Actualització de la llista de paraules. 200 paraules en Castellà i 200 més en Català.
 
- Incorporació d'un historial de partides, on es poden veure totes les partides i rondes de l'usuari.

### **Descripció del joc**
Aquest projecte és una versió del joc mundialment conegut amb el nom de **'Wordle'** executat per la terminal del propi programa.

**La versió inclou colors i emoticones**


### **Instal·lació i execució del projecte**
Primer de tot anem a internet i descàrreguem **intellij idea**.

Un cop descàrregat fem git clone a la terminal de linux per poder jugar en aquesta versió:

```sh
$ git clone https://gitlab.com/gerard.sanchez.7e6/wordle_gerardsanchez.git
```

I per últim, obre **Intellj** per poder executar el joc.

Executa aquest arxiu:
>Wordle.kt

### **Instruccions del joc**
L'obectiu és molt simple, endevinar la paraula oculta. La paraula té 5 lletres i tens 6 intents per endevinar-la.

Només és pot introduir una paraula de 5 lletres.

A cada ronda, el joc, pinta el fons de cada lletra indicant si la lletra es troba o no a la paraula i si es troba en la posició correcta.

- Si el fons de la lletra es de color verd significa que està dins de la paraula i a la posició correcte.

- Si el fons de la lletra es de color groc significa que està dins de la paraula, pero no a la seva posició.

- Si el fons de la lletra es de color gris significa que no està dins de la paraula.

El joc acaba quan perds tots els intents i no has encertat la paraula oculta o quan encertes la paraula en qualsevol  moment de la partida.

### **Autors**
- Gerad Sanchez ([Gitlab](https://gitlab.com/gerard.sanchez.7e6))


### **License**
Visualitza la llicència [License](LICENSE)


