import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class WordleKtTest {

    //gameMenu()
    @Test
    fun checkIfPrintCatalanMenu(){
        assertEquals("\n                                                  --------------- MENÚ --------------\n" +
                "                                                      1 -> Com es juga / Normes\n" +
                "                                                      2 -> Començar a jugar\n" +
                "                                                      3 -> Historial de partides\n" +
                "                                                      4 -> Canviar idioma\n" +
                "                                                      5 -> Sortir del joc\n" +
                "                                                  -----------------------------------\n",catalanMenu())
    }
    @Test
    fun checkIfPrintSpanishMenu(){
        assertEquals("\n                                                  --------------- MENÚ --------------\n" +
                "                                                      1 -> Cómo se juega / Normas\n" +
                "                                                      2 -> Empezar a jugar\n" +
                "                                                      3 -> Historial de partidas\n" +
                "                                                      4 -> Cambiar idioma\n" +
                "                                                      5 -> Salir del juego\n" +
                "                                                  -----------------------------------\n",spanishMenu())
    }

    // gameRules()

    @Test
    fun checkIfPrintCatalanGameRules() {
        assertEquals(
            "------------------------------------------------------------- BENVINGUT/DA -----------------------------------------------------------------\n\n"+

                    "L'obectiu és molt simple, endevinar la paraula oculta. La paraula té 5 lletres i tens 6 intents per endevinar-la."+
                    "\n${redColor}NOMÉS$reset és pot introduir una paraula de 5 lletres." +
                    "\nA cada ronda, el joc, pinta el fons de cada lletra indicant si la lletra es troba o no a la paraula i si es troba en la posició correcta.\n"+
                    "\nSi el fons de la lletra es de color $backgroundGreen VERD $reset significa que està dins de la paraula i a la posició correcte."+
                    "\nSi el fons de la lletra es de color $backgroundYellow GROC $reset significa que està dins de la paraula, pero no a la seva posició."+
                    "\nSi el fons de la lletra es de color $backgroundGrey GRIS $reset significa que $redColor NO$reset està dins de la paraula.\n\n"+

                    "--------------------------------------------------------------- JUGUEM? --------------------------------------------------------------------\n", catalanGameRules()
        )
    }
    @Test
    fun checkIfPrintSpanishGameRules() {
        assertEquals("------------------------------------------------------------- BIENVENIDO/DA ----------------------------------------------------------------\n\n"+

                "El objetivo es muy simple, acertar la palabra oculta. La palabra contiene 5 letras i tienes 6 intentos para acertarla."+
                "\n${redColor}SOLO$reset se puede introducir una palabra de 5 letras." +
                "\nA cada ronda, el juego, colorea el fondo de cada letra indicando si la letras se encuentra o no dentro de la palabra oculta " +
                "\ny si está en la posición correcta.\n"+
                "\nSi el fondo de la letra es de color $backgroundGreen VERDE $reset significa que se encuentra dentro de la palabra y posición correcta."+
                "\nSi el fons de la letra es de color $backgroundYellow AMARILLO $reset significa que se encuentra dentro de la palabra, pero no en su posición."+
                "\nSi el fons de la letra es de color $backgroundGrey GRIS $reset significa que $redColor NO$reset se necuentra dentro de la palabra.\n\n"+

                "--------------------------------------------------------------- JUGAMOS? -------------------------------------------------------------------\n",spanishGameRules()
            , catalanGameRules()
        )
    }


    // printAbecedary(mutableListOf()

    @Test
    fun checkIfPrintAbecedary() {
        assertEquals(
            "  A    B    C    Ç    D    E    F    G    H    I    J    K    L    M    N    O    P    Q    R    S    T    U    V    W    X    Y    Z  ",
            printAbecedary(
                mutableListOf(
                    "  A  ",
                    "  B  ",
                    "  C  ",
                    "  Ç  ",
                    "  D  ",
                    "  E  ",
                    "  F  ",
                    "  G  ",
                    "  H  ",
                    "  I  ",
                    "  J  ",
                    "  K  ",
                    "  L  ",
                    "  M  ",
                    "  N  ",
                    "  O  ",
                    "  P  ",
                    "  Q  ",
                    "  R  ",
                    "  S  ",
                    "  T  ",
                    "  U  ",
                    "  V  ",
                    "  W  ",
                    "  X  ",
                    "  Y  ",
                    "  Z  "
                )
            )
        )
    }

    @Test
    fun checkIfPrintAbecedrayWhithBackgroundStyle() {
        assertEquals(
            "  $backgroundGreen A$reset    C    Ç    D    E    F    G    H    I    J    K    L    M    N    O    P    Q    R    S    T    U    V    W    X    Y    Z  ",
            printAbecedary(
                mutableListOf(
                    "  $backgroundGreen A$reset  ",
                    "  C  ",
                    "  Ç  ",
                    "  D  ",
                    "  E  ",
                    "  F  ",
                    "  G  ",
                    "  H  ",
                    "  I  ",
                    "  J  ",
                    "  K  ",
                    "  L  ",
                    "  M  ",
                    "  N  ",
                    "  O  ",
                    "  P  ",
                    "  Q  ",
                    "  R  ",
                    "  S  ",
                    "  T  ",
                    "  U  ",
                    "  V  ",
                    "  W  ",
                    "  X  ",
                    "  Y  ",
                    "  Z  "
                )
            )
        )
    }

    @Test
    fun checkIfPrintAbecedrayWhithMultipleBackgroundStyles() {
        assertEquals(
            "  $backgroundGreen A$reset    C    Ç    D    E    F    G    $backgroundGrey H$reset    I    J    K    L    M    $backgroundYellow N$reset    O    P    Q    R    S    T    U    V    W    X    Y    Z  ",
            printAbecedary(
                mutableListOf(
                    "  $backgroundGreen A$reset  ",
                    "  C  ",
                    "  Ç  ",
                    "  D  ",
                    "  E  ",
                    "  F  ",
                    "  G  ",
                    "  $backgroundGrey H$reset  ",
                    "  I  ",
                    "  J  ",
                    "  K  ",
                    "  L  ",
                    "  M  ",
                    "  $backgroundYellow N$reset  ",
                    "  O  ",
                    "  P  ",
                    "  Q  ",
                    "  R  ",
                    "  S  ",
                    "  T  ",
                    "  U  ",
                    "  V  ",
                    "  W  ",
                    "  X  ",
                    "  Y  ",
                    "  Z  "
                )
            )
        )
    }

    // sentenceAttemptsRemaining( intents : Int)

    @Test
    fun checkSentenceIfIntentsIs1() {
        assertEquals(
            "   Escriu una paraula de 5 lletres. \u001B[31m1 INTENTS RESTANTS \u001B[0m\n",
            sentenceAttemptsRemaining(1)
        )
    }

    @Test
    fun checkSentenceIfIntentsIs5() {
        assertEquals(
            "   Escriu una paraula de 5 lletres. \u001B[31m5 INTENTS RESTANTS \u001B[0m\n",
            sentenceAttemptsRemaining(5)
        )
    }

    // printHistory(MutableList<MutableList<String>>)
    @Test
    fun checkIfPrintWordsHistory() {
        assertEquals(" \n" +
                "\t\t\t\t\t\t\t\t\t\t\t\t\t   ✖     ✖     ✖     ✖     ✖   \n" +
                "\n" +
                "\t\t\t\t\t\t\t\t\t\t\t\t\t   ✖     ✖     ✖     ✖     ✖   \n" +
                "\n" +
                "\t\t\t\t\t\t\t\t\t\t\t\t\t   ✖     ✖     ✖     ✖     ✖   \n" +
                "\n" +
                "\t\t\t\t\t\t\t\t\t\t\t\t\t   ✖     ✖     ✖     ✖     ✖   \n" +
                "\n" +
                "\t\t\t\t\t\t\t\t\t\t\t\t\t   ✖     ✖     ✖     ✖     ✖   \n" +
                "\n" +
                "\t\t\t\t\t\t\t\t\t\t\t\t\t   ✖     ✖     ✖     ✖     ✖   \n",
            printHistory(MutableList(6) { MutableList(5) { "  ✖  " } })
        )
    }

    // letterCounter(userword)
    @Test
    fun checkWordLenghtWithNormalWord() {
        assertEquals(5, letterCounter("color"))
    }

    @Test
    fun checkWordLenghtWithCapitalLetters() {
        assertEquals(7, letterCounter("SETMANA"))
    }

    @Test
    fun checkWordLenghtWithCapitalAndLowLetters() {
        assertEquals(8, letterCounter("AvInGuDa"))
    }


    // fun gameOverSentence( userWord, randomWord, intents)
    @Test
    fun checkIfSameWordsAnd0Intent() {
        assertEquals(true,gameOverSentence("norma","norma",0))

    }
    @Test
    fun checkIfDifferentsWordsAnd0Intent() {
        assertEquals(false,gameOverSentence("dones","ronda",0))

    }
    @Test
    fun checkIfDifferentsWordsANd3Intents() {
        assertEquals(false,gameOverSentence("conte","mapes",3))

    }

    // fun questionPlayAgain(playAgain)
    @Test
    fun checkIfAnswerIsNo() {
        assertEquals(false,questionPlayAgain("NO"))

    }
    @Test
    fun checkIfAnswerIsSi() {
        assertEquals(false,questionPlayAgain("SI"))

    }
    @Test
    fun checkIfAnswerISDIsfferentThanSiNo() {
        assertEquals(true,questionPlayAgain("a"))

    }

}


