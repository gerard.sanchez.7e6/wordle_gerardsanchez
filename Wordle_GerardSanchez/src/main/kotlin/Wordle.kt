/**
 * @author Gerard Sanchez Preto
 * @version 1.3 12-02-2023
 */

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.appendText
import kotlin.io.path.readLines
import kotlin.system.exitProcess

const val backgroundGreen = "\u001B[42m"
const val backgroundYellow = "\u001B[43m"
const val backgroundGrey = "\u001B[47m"
const val redColor = "\u001B[31m"
const val greenColor = "\u001B[32m"
const val reset = "\u001B[0m"

val catalanWordsFile = Path("Wordle_GerardSanchez/src/main/DataWords/catalanWords")
val spanishWordsFile = Path("Wordle_GerardSanchez/src/main/DataWords/spanishWords")
val userHistoryFile = Path ("Wordle_GerardSanchez/src/main/History/userHistory")
val scanner = Scanner(System.`in`)
var language = 0


/**
 * Funció que crea el menú del joc en CASTELLANO.
 *
 * @return  Un String del menú del joc.
 */
fun spanishMenu() : String{
    return  "\n                                                  --------------- MENÚ --------------\n" +
            "                                                      1 -> Cómo se juega / Normas\n" +
            "                                                      2 -> Empezar a jugar\n" +
            "                                                      3 -> Historial de partidas\n" +
            "                                                      4 -> Cambiar idioma\n" +
            "                                                      5 -> Salir del juego\n" +
            "                                                  -----------------------------------\n"
}

/**
 * Funció que crea el menú del joc en CATALÀ
 *
 * @return  Un String del menú del joc.
 */
fun catalanMenu() : String{
    return  "\n                                                  --------------- MENÚ --------------\n" +
            "                                                      1 -> Com es juga / Normes\n" +
            "                                                      2 -> Començar a jugar\n" +
            "                                                      3 -> Historial de partides\n" +
            "                                                      4 -> Canviar idioma\n" +
            "                                                      5 -> Sortir del joc\n" +
            "                                                  -----------------------------------\n"
}

/**
 * Funció que genera el menú principal del joc. Segons el número que l'usuari introdueixi
 * podrà fer diverses funcions.
 */
fun generalMenu(){
    do{
        repeat(68){ print(" ")}
        val userChoose = scanner.nextLine()
        println()

        when(userChoose) {
            "1" -> {
                if (language == 0) {
                    println(catalanGameRules())
                    println(catalanMenu())
                } else {
                    println(spanishGameRules())
                    println(spanishMenu())
                }
            }

            "3" -> {

                for ( i  in 0 ..userHistoryFile.readLines().lastIndex){
                    println(userHistoryFile.readLines()[i])
                }

                if (language == 0) {
                    println(catalanMenu())
                } else {
                    println(spanishMenu())
                }
            }

            "4" -> {
                do {
                    println("                                                  -----------------------------------\n"+
                            "                                                             1 -> CATAlÀ\n" +
                            "                                                             2 -> CASTELLANO\n" +
                            "                                                  -----------------------------------\n"
                    )
                    repeat(68){ print(" ")}
                    val userLanguage = scanner.nextLine()


                    if (userLanguage == "1") {
                        println(catalanMenu())
                        language = 0
                    }
                    if (userLanguage == "2") {
                        println(spanishMenu())
                        language = 1
                    }

                } while (userLanguage != "1" && userLanguage != "2")

            }

            "5" -> {
                exitProcess(0)
            }
        }

    }while(userChoose != "2" )

}


/**
 * Funció que crea les normes principals del joc en CATALÀ.
 *
 * @return  Un String de les normes del joc.
 */
fun catalanGameRules () : String {
    return "------------------------------------------------------------- BENVINGUT/DA -----------------------------------------------------------------\n\n"+

            "L'obectiu és molt simple, endevinar la paraula oculta. La paraula té 5 lletres i tens 6 intents per endevinar-la."+
            "\n${redColor}NOMÉS$reset és pot introduir una paraula de 5 lletres." +
            "\nA cada ronda, el joc, pinta el fons de cada lletra indicant si la lletra es troba o no a la paraula i si es troba en la posició correcta.\n"+
            "\nSi el fons de la lletra es de color $backgroundGreen VERD $reset significa que està dins de la paraula i a la posició correcte."+
            "\nSi el fons de la lletra es de color $backgroundYellow GROC $reset significa que està dins de la paraula, pero no a la seva posició."+
            "\nSi el fons de la lletra es de color $backgroundGrey GRIS $reset significa que $redColor NO$reset està dins de la paraula.\n\n"+

            "--------------------------------------------------------------- JUGUEM? --------------------------------------------------------------------\n"

}
/**
 * Funció que crea les normes principals del joc en CASTELLANO.
 *
 * @return  Un String de les normes del joc.
 */
fun spanishGameRules () : String{
    return "------------------------------------------------------------- BIENVENIDO/DA ----------------------------------------------------------------\n\n"+

            "El objetivo es muy simple, acertar la palabra oculta. La palabra contiene 5 letras i tienes 6 intentos para acertarla."+
            "\n${redColor}SOLO$reset se puede introducir una palabra de 5 letras." +
            "\nA cada ronda, el juego, colorea el fondo de cada letra indicando si la letras se encuentra o no dentro de la palabra oculta " +
            "\ny si está en la posición correcta.\n"+
            "\nSi el fondo de la letra es de color $backgroundGreen VERDE $reset significa que se encuentra dentro de la palabra y posición correcta."+
            "\nSi el fons de la letra es de color $backgroundYellow AMARILLO $reset significa que se encuentra dentro de la palabra, pero no en su posición."+
            "\nSi el fons de la letra es de color $backgroundGrey GRIS $reset significa que $redColor NO$reset se necuentra dentro de la palabra.\n\n"+

            "--------------------------------------------------------------- JUGAMOS? -------------------------------------------------------------------\n"
}
/**
 * Funció que crea / mostra les lletres del abecedari d'una forma atractiva / vistosa per l'usuari
 *
 * @param abecedary Llista que guarda les lletres del abecedari
 * @return  Un String de les lletres del abecedari.
 */
fun printAbecedary ( abecedary: MutableList<String> ): String {
    var string= ""
    for (i in 0..abecedary.lastIndex) {
        string += abecedary[i]
    }
    return string

}

/**
 * Funció que segons els intents restants de l'usuari, durant la partida, et retorna
 * una frase o altre.
 *
 * @param intents Variable que guarda els intents restants de l'usuari.
 * @return  Un String indicant el que ha d'escriure l'usuari.
 */
fun sentenceAttemptsRemaining( intents : Int) : String{
    return if (language == 0){
        "   Escriu una paraula de 5 lletres. $redColor$intents INTENTS RESTANTS $reset\n"
    }
    else{
        "   Escribe una palabra de 5 letras. $redColor$intents INTENTOS  RESTANTES $reset \n"
    }
}

/**
 * Funció que agafa la paraula introduida per l'usuari i la compara per caràcter a caràcter
 * amb la paraula generada aleatoriament, seguidament va afegint el caracter comparat al historial
 * de la partida amb els seus sestils respectivament.
 *
 * @param userWord Paraula introduida per l'usuari.
 * @param randomWord Paraula generada aleatoriament.
 * @param round Variable que guarda la ronda actual.
 * @param wordsHistory LLista que guarda l'historial de paraules de l'usuari.
 * @param abecedary Llista que guarda les lletres del abecedari
 * @return  La funcio printHistory en forma d'String
 */

/*
// Intent de fer-ho amb lletres repetides
fun proba (userWord : String, randomWord : String, round : Int, wordsHistory : MutableList<MutableList<String>>, abecedary: MutableList<String>,aux : MutableList<String>, aux2 : MutableList<String>) : String {
for (i in 0..randomWord.lastIndex){
    aux2.add(randomWord[i].toString())
}

    for (i in 0..randomWord.lastIndex) {

        if (userWord[i] == randomWord[i]) {
            wordsHistory[round][i] = " $backgroundGreen ${userWord[i].uppercase()} $reset "


            for (j in 0..abecedary.lastIndex) {
                if ("  ${userWord[i].uppercase()}  " == abecedary[j]) {
                    abecedary[j] = " $backgroundGreen ${userWord[i].uppercase()} $reset "
                }
                if (" $backgroundYellow ${userWord[i].uppercase()} $reset " == abecedary[j]) {
                    abecedary[j] = " $backgroundGreen ${userWord[i].uppercase()} $reset "
                }
            }
            aux2.remove("${userWord[i]}")
        }
        else{
            aux.add(userWord[i].toString())

            for (j in 0..aux.lastIndex){
                if (aux[j] in aux2){

                    wordsHistory[round][i] = " $backgroundYellow ${aux[j].uppercase()} $reset "
                    for (k in 0..abecedary.lastIndex) {
                        if ("  ${aux[j].uppercase()}  " == abecedary[k]) {
                            abecedary[k] = " $backgroundYellow ${aux[j].uppercase()} $reset "
                        }
                    }

                    aux.remove("${userWord[i]}")
                    aux2.remove("${userWord[i]}")
                }
                 else if (aux[j] !in aux2){
                    wordsHistory[round][i] = " $backgroundGrey ${aux[j].uppercase()} $reset "
                    for (k in 0..abecedary.lastIndex) {
                        if ("  ${aux[j].uppercase()}  " == abecedary[k]) {
                            abecedary[k] = " $backgroundGrey ${aux[j].uppercase()} $reset "
                        }
                    }
                }
            }
        }

    }
    return printHistory(wordsHistory)
 */
fun printWordsHistory (userWord : String, randomWord : String, round : Int, wordsHistory : MutableList<MutableList<String>>, abecedary: MutableList<String> ) : String {

    for (i in 0..randomWord.lastIndex) {
        if (userWord[i] == randomWord[i]) {
            wordsHistory[round][i] = " $backgroundGreen ${userWord[i]} $reset "
            for (j in 0..abecedary.lastIndex) {
                if ("  ${userWord[i]}  " == abecedary[j]) {
                    abecedary[j] = " $backgroundGreen ${userWord[i]} $reset "
                }
                if (" $backgroundYellow ${userWord[i]} $reset " == abecedary[j]) {
                    abecedary[j] = " $backgroundGreen ${userWord[i]} $reset "
                }
            }

        } else if (userWord[i] in randomWord) {
            wordsHistory[round][i] = " $backgroundYellow ${userWord[i]} $reset "
            for (j in 0..abecedary.lastIndex) {
                if ("  ${userWord[i]}  " == abecedary[j]) {
                    abecedary[j] = " $backgroundYellow ${userWord[i]} $reset "
                }
            }

        } else if (userWord !in randomWord) {
            wordsHistory[round][i] = " $backgroundGrey ${userWord[i]} $reset "
            for (j in 0..abecedary.lastIndex) {
                if ("  ${userWord[i]}  " == abecedary[j]) {
                    abecedary[j] = " $backgroundGrey ${userWord[i]} $reset "
                }
            }
        }
    }
    return printHistory(wordsHistory)
}

/**
 * Funció que retorna la llargada de la paraula introduida per l'usuari.
 *
 * @param userWord Paraula introduida per l'usuari.
 * @return  Un Int amb la llargada de la paraula introduida.
 */
fun letterCounter(userWord: String): Int{
    return userWord.length
}

/**
 * Funció que crea / mostra les lletres de la paraula introduida per l'usuari
 * d'una forma atractiva / vistosa.
 *
 * @param wordsHistory Llista que guarda les lletres de la paraula introduida per l'usuari
 * @return  Un String del historial de paraules de la partida.
 */
fun printHistory (wordsHistory : MutableList<MutableList<String>>) : String {
    var string = " "
    for (i in 0 until 6) {
        string += "\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t "
        for (j in 0 until 5) {
            string += "${wordsHistory[i][j]} "
        }
        string += "\n"
    }
    return string
}

/**
 * Funció que compara la paraula introduida per l'usuari, la paraula generada aleatoriament
 * per el programa i els intents restants per determinar si has gaunyat o perdut la partida.
 *
 * @param userWord Paraula introduida per l'usuari.
 * @param randomWord Paraula generada aleatoriament.
 * @param intents Variable que guarda els intents restants de l'usuari.
 * @return  Un Boolean segons si has guanyat o perdut la partida.
 */
fun gameOverSentence(userWord: String, randomWord: String, intents: Int): Boolean{
    if (userWord == randomWord && intents >= 0){
        return true
    }
    return if (userWord != randomWord &&  intents == 0){
        false
    }
    else{
        false
    }
}

/**
 * Funció que nomes es repeteix si l'usuari al introduir el que l'hi demanen s'equivoca
 * i determinar si vol acabar de jugar o tornar a començar el joc.
 *
 * @param playAgain Variable que guarda un Boolean.
 * @return  Un Boolean per saber si s'ha de repetir la pregunta.
 */
fun questionPlayAgain(playAgain : String): Boolean {
     if(playAgain == "NO"){
        return false
    }
     if (playAgain == "SI") {
         return false
     }
    return true

}

/**
 * Funció que afegeix al historial de partides, la partida jugada per l'usuari.
 * Amb la data actual i hora actuals
 *
 * @param round Variable que guarda la ronda actual.
 * @param randomWord Paraula generada aleatoriament.
 * @param userName  Variable que guarda el nom de l'usuari.
 * @param intents Variable que guarda els intents restants de l'usuari.
 * @param wordsHistory LLista que guarda l'historial de paraules de l'usuari.
 */
fun addToHistoryMatches(round: Int, randomWord: String, userName: String,intents: Int, wordsHistory: MutableList<MutableList<String>>){

    val dateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd MMM yyyy, hh:mm:ss a"))

    if  (intents > 0){
        userHistoryFile.appendText("                          L'usuari \"$userName\" ha encertat la paraula $greenColor\"$randomWord\"$reset en $round intents. ($dateTime)\n " +
                "${printHistory(wordsHistory)}\n" +
                "                          ----------------------------------------------------------------------------------------\n")
    }
    else{
        userHistoryFile.appendText("                          L'usuari \"$userName\" no ha encertat la paraula $redColor\"$randomWord\"$reset en $round intents. ($dateTime)\n " +
                "${printHistory(wordsHistory)}\n" +
                "                          ----------------------------------------------------------------------------------------\n")

    }

}
fun main() {
    repeat(57){ print(" ")}
    print("INTRODUEIX EL TEU NOM: ")

    val userName = scanner.nextLine()

    println(catalanMenu())

    do {
        generalMenu()

        val wordsHistory = MutableList(6) { MutableList(5) { "  ✖  " } }
        val abecedary = mutableListOf(
            "  A  ",
            "  B  ",
            "  C  ",
            "  Ç  ",
            "  D  ",
            "  E  ",
            "  F  ",
            "  G  ",
            "  H  ",
            "  I  ",
            "  J  ",
            "  K  ",
            "  L  ",
            "  M  ",
            "  N  ",
            "  Ñ  ",
            "  O  ",
            "  P  ",
            "  Q  ",
            "  R  ",
            "  S  ",
            "  T  ",
            "  U  ",
            "  V  ",
            "  W  ",
            "  X  ",
            "  Y  ",
            "  Z  "
        )
        var intents = 6
        var round = 0
        var userWord: String
        val randomWord :String

        if(language == 0){
             randomWord = catalanWordsFile.readLines().random().uppercase()

        }
        else{
            randomWord = spanishWordsFile.readLines().random().uppercase()

        }


        // Comença el bucle d'intents del usuari
        do {
            //Pintem la llista $abededary
            repeat(140) { print("-") }
            println("\n${printAbecedary(abecedary)}")

            repeat(140) { print("-") }
            println()

            repeat(10) { print("\t") }
            print(sentenceAttemptsRemaining(intents))

            repeat(16) { print("\t") }
            print(" ")
            userWord = scanner.next().uppercase()

            // Comprobació de la quantitat de lletres que conté la paraula introduida per l'usuari.
            if (letterCounter(userWord) != 5 && language == 0) {
                print("\t\t\t\t\t\t\t\t   $redColor ERROR. Enrecordat que la paraula introduida ha de ser de 5 lletres. $reset\n")

            }
            else if (letterCounter(userWord) != 5 && language == 1) {
                print("\t\t\t\t\t\t\t\t   $redColor ERROR. Acuerdate que la palabra introducida tiene que contener 5 letras. $reset\n")
            }
            else {
                // Pintem la llista $wordsHistory
                println(printWordsHistory(userWord, randomWord, round, wordsHistory, abecedary))
                intents--
                round++
            }

        } while (userWord != randomWord && intents != 0)

        // Final del joc
        if (gameOverSentence(userWord, randomWord, intents)) {
            repeat(12) { print("\t") }
            repeat(39) { print("-") }
            println()
            repeat(12) { print("\t") }
            if (language == 0) {
                print(" FELICITATS!!! HAS ACERTAT LA PARAULA\n")
            } else {
                print(" FELICIDADES!!! HAS ACERTADO LA PALABRA\n")

            }
            repeat(12) { print("\t") }
            repeat(39) { print("-") }
            println()

        }
        if (!gameOverSentence(userWord, randomWord, intents)) {
            repeat(12) { print("\t") }
            repeat(39) { print("-") }
            println()
            repeat(12) { print("\t") }
            if (language == 0) {
                print("  HAS PERDUT. LA PARAULA ERA :$greenColor $randomWord $reset\n")
            } else {
                print("  HAS PERDIDO. LA PALABRA ERA :$greenColor $randomWord $reset\n")
            }
            repeat(12) { print("\t") }
            repeat(39) { print("-") }
            println()

        }
        addToHistoryMatches(round, randomWord, userName, intents,wordsHistory)



        // Pregunta final per si l'usuari vol tornar a jugar
        var playAgain: String

        do {
            repeat(13) { print("\t") }
            if(language == 0){
                println("  Vols tornar a jugar?$greenColor SI $reset/$redColor NO $reset ")
            }
            else{
                println("  Quieres volver a jugar?$greenColor SI $reset/$redColor NO $reset ")
            }
            repeat(16) { print("\t") }
            print("  ")
            playAgain = scanner.next().uppercase()


        } while (questionPlayAgain(playAgain))
        if (language == 0 && playAgain == "SI") {
            println(catalanMenu())
        }
        if (language == 1 && playAgain == "SI") {
            println(spanishMenu())
        }

    } while (playAgain == "SI")


}